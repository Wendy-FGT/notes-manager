const mongoose = require('mongoose')
const validator = require('validator')
const User = require('../models/user')

const noteSchema = new mongoose.Schema(
    {
        title:{
            type: String,
            required: true,
            trim: true,
        }, 
        body:{
            type: String,
        },
        author :{
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        }
    },
    {
        timestamps: true
    }
    
)
const Note = mongoose.model('Note', noteSchema )

module.exports = Note