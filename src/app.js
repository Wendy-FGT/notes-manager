const express = require('express')
require('./db/mongoose')
const userRouter = require('./routers/user')
const noteRouter = require('./routers/notes')

const app = express()

app.use(express.json())
app.use(userRouter)
app.use(noteRouter)

module.exports = app