const express = require('express')
const router = new express.Router()
const User = require('../models/user')
const auth = require('../middleware/auth')
const multer = require('multer')


//register new user
router.post('/v1/auth/register', async (req, res) => {
    const user = new User(req.body)
    
    try {
        await user.save()
        const token  = await user.generateAuthToken()
        res.status(201).send({user, token})
    } catch (e){
       res.status(400).send(e)
       console.log(e)
    }
   
})

//login new user
router.post('/v1/auth/login',  async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email,req.body.password)
        const token = await user.generateAuthToken()
       
        res.send({user, token})
    }catch(e){
        res.sendStatus(400)
        console.log(e)
    }
})

// router.post('/users/logout', auth, async (req, res) => {
//     try {
//         req.user.tokens = req.user.tokens.filter((token) => {
//             return token.token !== req.token
//         })
//         await req.user.save()
//         res.send()
//     }catch (e) {
//         res.sendStatus(500)
//     }
// })

// router.post('/users/logoutAll', auth, async(req, res) => {
//     try{
//         req.user.tokens = []
//         await req.user.save()
//         res.sendStatus(200)
//     }catch (e) {
//         res.sendStatus(500)
//     }
// })


module.exports = router