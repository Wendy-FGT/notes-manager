const express = require('express')
const router = new express.Router()
const Note = require('../models/notes')
const auth = require('../middleware/auth')


//create a note
router.post('/v1/notes', auth, async (req, res) => {
    const note = new Note({
        ...req.body,
        author: req.user._id
    })

    try {
        await note.save()
        res.send(note)
    }
    catch(error) {
        res.status(400).send(error)
    }
})

//GET /notes?title
//add support for pagination using limit and skip
//GET /notes?limit=10&skip=20
//ADD SUPPORT FOR SORT
//GET /notes?sortBy=createdAt_asc//notes?sortBy=createdAt_desc(descending -1; ascending 1)
router.get('/v1/notes', auth, async (req,res) => {
    const match = {}
    const sort = {}

    if(req.query.completed){
        match.completed = req.query.completed === 'true'
    }

    if(req.query.sortBy){
        const parts = req.query.sortBy.split('_')
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
    }
    try{
        // const tasks = await Task.find({author: req.user._id})
        // if(tasks.length === 0){
        //     return res.send('You have no tasks.')
        // }
        await req.user.populate({
            path: 'notes',
            match,
            options:{
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort
            }
        })
        res.send(req.user.notes)
    } catch(e) {
        res.status(500).send()
    }
})

//get note by id
router.get('/v1/notes/:id', auth, async (req,res) => {
    const _Id = req.params._id
    try {
        const note = await Note.findOne({_id: req.params.id, author:req.user._id})
        if(!note){
            return res.status(404).send()
        }
        res.send(note)
    }catch (e) {
        res.status(404).send()
    }
})

//update a note 
router.put('/v1/notes/:id', auth, async (req, res) => {
    const allowedUpdates = ["title", "body"]
    const updates = Object.keys(req.body)
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
        if (!isValidOperation) {
            return res.status(400).send({error: "Invalid updates!"})
        }

        try{
            const note = await Note.findOne({_id: req.params.id, author:req.user._id})
            
            if(!note){
                return res.status(404).send()
            }

            updates.forEach((update) => {
                note[update] = req.body[update]
            })
            await note.save()
            res.send(note)
        }catch (e) {
            return res.status(404).send(e)
        }
})

//delete a note
router.delete('/v1/notes/:id', auth, async(req, res) =>{
    const _id = req.params.id

    try {
        const note = await Note.findOneAndDelete({_id, author:req.user._id})
        if(!note){
            return res.sendStatus(404)
        }
        res.send(note)
    }catch (e) {
        return res.sendStatus(500)
    }
})

module.exports = router