const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const User = require('../../src/models/user')
const Note = require('../../src/models/notes')

const userOneId = new mongoose.Types.ObjectId()
const userOne = {
    _id: userOneId,
    first_name: 'Chinny',
    last_name: 'Okolo',
    email:'chinny@example.com',
    password:'s0l1725!!!',
    tokens:[{
        token: jwt.sign({_id: userOneId}, process.env.JWT_SECRET)
    }]
}

const userTwoId = new mongoose.Types.ObjectId()
const userTwo = {
    _id: userTwoId,
    first_name: 'Olivia',
    last_name: 'Onuoha',
    email:'olivia@example.com',
    password:'s0l1725!!!',
    tokens:[{
        token: jwt.sign({_id: userTwoId}, process.env.JWT_SECRET)
    }]
}

const userThreeId = new mongoose.Types.ObjectId()
const userThree = {
    _id: userThreeId ,
    first_name: 'Solange',
    last_name: 'Onuoha',
    email:'sol@example.com',
    password:'s0l1725!!!',
    tokens:[{
        token: jwt.sign({_id: userTwoId}, process.env.JWT_SECRET)
    }]
}

const noteOne = {
    _id: new mongoose.Types.ObjectId(),
    title: 'Still studying',
    body: "Studying has made great ppole.",
    author: userThreeId
}

const noteTwo = {
    _id: new mongoose.Types.ObjectId(),
    title: 'Sleeping',
    body: "Sleep at least 7 hours a day",
    author: userOneId
}

const noteThree = {
    _id:new mongoose.Types.ObjectId(),
    title: 'Eating',
    body: "Balanced diet: protein, carbs, water, veg and sweets!",
    author: userThreeId
}

const setupDatabase = async () => {  
       //to test login(required in the database)
     await User.deleteMany()
     await Note.deleteMany()
  
    await new User(userOne).save()
   await new User(userTwo).save()
   await new Note(noteOne).save()
   await new Note(noteTwo).save()
   await new Note(noteThree).save()
   
}



module.exports = {
    userOneId,
    userOne,
    userTwo,
    userTwoId,
    userThree,
    userThreeId,
    noteOne,
    noteTwo,
    noteThree,
    setupDatabase
}