const request = require('supertest')
const app = require('../src/app')
const Note = require('../src/models/notes')
const { userOne, userTwo,userThree,noteOne,noteThree, noteTwo, setupDatabase } = require('./fixtures/db')

beforeEach(setupDatabase)

test('Should create note for user', async () => {
    const response = await request(app)
        .post('/v1/notes')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            title:"Hey",
            body: 'From my test'
        })
        .expect(200)
    const note = await Note.findById(response.body._id)
    expect(note).not.toBeNull()
})

test('All notes for user one', async () => {
 await request(app)
        .get('/v1/notes')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})


test('Should not fetch other users notes by id', async () => {
     await request(app)
    .get(`/v1/notes/${noteTwo._id}`)
    .set('Authorization', `Bearer ${userThree.tokens[0].token}`)
    .send()
    .expect(404)
})

test('Should not allow unauthorized user delete note not created by them', async () => {
     await request(app)
    .delete(`/v1/notes/${noteOne._id}`)
    .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
    .send()
    .expect(404)

    const note = await Note.findById(noteOne._id)
    expect(note).not.toBeNull()
})

test('Should not delete note if unauthenticated', async () => {
     await request(app)
    .delete(`/v1/notes/${noteOne._id}`)
    .send()
    .expect(401)

    const note = await Note.findById(noteOne._id)
    expect(note).not.toBeNull()
})

test('Should not create task with invalid description/completed', async () => {
   await request(app)
    .post('/v1/notes')
    .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
    .send({
        title:"",
        body:""
    })
    .expect(400)
})

test('Should not allow unauthorized user update note not created by them', async () => {
    const response = await request(app)
    .put(`/v1/notes${noteOne._id}`)
    .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
    .send()
    .expect(404)

    const note = await Note.findById(noteOne._id)
    expect(note).not.toBeNull()
})

