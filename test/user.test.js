const request = require('supertest')
const app = require('../src/app')
const User = require('../src/models/user')
const { response } = require('../src/app')
const {userOne, userOneId, setupDatabase} = require('./fixtures/db')

beforeEach(setupDatabase)

test('Should signup a new user', async () => {
    const response = await request(app).post('/v1/auth/register').send({
        first_name:'Chinwendu',
        last_name:'Onuoha',
        email:'chinwendu@example.com',
        password: 'MyPass777!!!'
    }).expect(201)

    //Assert that the database was changed correctly
    const user = await User.findById(response.body.user._id)
    expect(user).not.toBeNull()

    //Assert things about the response body
    expect(response.body).toMatchObject({
        user: {
            first_name: 'Chinwendu',
            last_name:'Onuoha',
            email:'chinwendu@example.com'
        },
        token: user.tokens[0].token
    })
    expect(user.password).not.toBe('MyPass777!!!')
})

test('Should login existing user', async () => {
   const response =  await request(app).post('/v1/auth/login').send({
        email:userOne.email,
        password: userOne.password
    }).expect(200)
    const user = await User.findById(userOneId)
    expect(response.body.token).toBe(user.tokens[1].token)
})

test('Should not login nonexistent user', async () => {
    await request(app).post('/v1/auth/login').send({
        email:userOne.email,
        password:'userOnepassword'
    }).expect(400)
})


test('Should not signup user with invalid name/email/password', async () => {
     await request(app)
    .post('/v1/auth/register')
    .send({
        first_name:493020,
        last_name:"",
        email: "",
        password:""
    }).expect(400)
})